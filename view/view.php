<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Calculate Cash</li>
  </ol>
</nav>

<div class="container">
  <div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 100</label>
      <div>
        <input class="form-control form-control-lg input-note" type="number" name="100">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 50</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="50">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 20</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="20">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 10</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="10">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 5</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="5">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 1</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="1">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 0.50</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="cent50">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 0.20</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="cent20">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 0.10</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="cent10">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">RM 0.05</label>
      <div>
        <input class="form-control form-control-lg input-note" type="text" name="cent5">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-5 col-form-label col-form-label-lg">Total</label>
      <div>
        <input class="form-control form-control-lg input-note" type="number" name="total">
      </div>
    </div>
  </div>
  <button type="button" id="calcBtn" class="btn btn-secondary">Calculate</button>
</div>

<script>
  document.getElementById("calcBtn").addEventListener("click", function() {
    console.log("run");
    console.log(document.getElementsByName("total"));
    console.log(document.querySelector('[name="total"]'));
    rm100 = parseFloat(document.querySelector('[name="100"]').value);
    rm50 = parseFloat(document.querySelector('[name="50"]').value);
    rm20 = parseFloat(document.querySelector('[name="20"]').value);
    rm10 = parseFloat(document.querySelector('[name="10"]').value);
    rm5 = parseFloat(document.querySelector('[name="5"]').value);
    rm1 = parseFloat(document.querySelector('[name="1"]').value);
    cent50 = parseFloat(document.querySelector('[name="cent50"]').value);
    cent20 = parseFloat(document.querySelector('[name="cent20"]').value);
    cent10 = parseFloat(document.querySelector('[name="cent10"]').value);
    cent5 = parseFloat(document.querySelector('[name="cent5"]').value);
    total = ((100*rm100) + (50*rm50) + (20*rm20) + (10*rm10) + (5*rm5) + rm1 + (0.5*cent50) + (0.2*cent20) + (0.1*cent10) + (0.05*cent5));
    document.querySelector('[name="total"]').value = total;
  });
</script>