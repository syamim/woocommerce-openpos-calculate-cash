<?php
/*
Plugin Name: Woocommerce OpenPos Calculate Cash
Plugin URI: http://fatimahfrozen.com
Description: A Simple Addon App for OpenPOS to calculate cash in register.
Author: syamim727@gmail.com
Author URI: http://fatimahfrozen.com
Version: 1.0
Text Domain: openpos-app
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/

define('OPENPOS_APP_DIR',plugin_dir_path(__FILE__));
define('OPENPOS_APP_URL',plugins_url('woocommerce-openpos-calculate-money'));


if(!class_exists('OP_App_Abstract'))
{
        $openpos_dir = dirname(rtrim(OPENPOS_APP_DIR , '/') ).'/woocommerce-openpos';
        require_once $openpos_dir.'/lib/abtract-op-app.php';
}

require_once OPENPOS_APP_DIR.'/extend-class.php';

$myApp = new MyOpApp();