<?php

class MyOpApp extends OP_App_Abstract implements OP_App {
  public $key = 'calculate_money'; // unique
  public $name = 'Calculate Money App';
  public $thumb = OPENPOS_APP_URL.'/app.png';
  public function render()
  {
    $session = $this->get_session();
    require_once OPENPOS_APP_DIR.'/view/view.php';
  }
}